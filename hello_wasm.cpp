#include <opencv2/opencv.hpp>
#include <emscripten/bind.h>

#include <filesystem>
#include <iostream>

auto getCvMatFromWasmHeap(const int imageBufferOffset, const int width, const int height)
{
  std::cout << "imageBufferOffset: " << imageBufferOffset << ", width: " << width << ", height: " << height << std::endl;
  if(imageBufferOffset == 0) {
    return cv::Mat{};
  }

  auto image = cv::Mat(height, width, CV_8UC4, reinterpret_cast<uint8_t*>(imageBufferOffset));
  cv::Mat convertedImage;
  cv::cvtColor(image, convertedImage, cv::COLOR_RGBA2BGR );

  return convertedImage;
}

auto imageStats(const auto& image)
{
  const auto mean = cv::mean(image);

  // Results using opencv.js directly:
  // Mean: 155.11690028596877, 136.49754582532464, 134.88651024330787
  std::cout << "Mean: " << mean[0] << ", " << mean[1] << ", " << mean[2] << std::endl;

  return mean;
}

int processImage(const int imageBufferOffset, const int width, const int height)
{
  const auto image = getCvMatFromWasmHeap(imageBufferOffset, width, height);

  return imageStats(image)[0];
}

namespace {
  using namespace emscripten;
  EMSCRIPTEN_BINDINGS(my_module) {
      function("processImage", &processImage);
  }
}
