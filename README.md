# Hello Wasm

Building hello_wasm.wasm:
`mkdir build && cd build && emcmake cmake -DCMAKE_BUILD_TYPE=Release ..`

Running server:
`emrun --port 8080 .` (from build directory)

Building OpenCV.js:
does not work for me as shown in https://docs.opencv.org/4.x/d4/da1/tutorial_js_setup.html
python script does not understand arguments passed by emcmake

`python3 ./opencv/platforms/js/build_js.py --emscripten_dir=/usr/local/Cellar/emscripten/3.1.3/libexec  build_wasm --build_wasm --enable_exception --simd`
